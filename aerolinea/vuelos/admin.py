from django.contrib import admin
from .models import Vuelo, Aeropuerto

# Register your models here.
admin.site.register(Vuelo)
admin.site.register(Aeropuerto)