from django.urls import path, include
from . import views

app_name = 'vuelos'
urlpatterns = [
    
    path('', views.index, name='vuelos/index.html'),
    path('<int:vuelo_id>', views.vuelo, name='vuelos/vuelo.html'),

]
