from django.shortcuts import render
from .models import Vuelo, Aeropuerto

# Create your views here.
def index(request):
     return render(request, 'vuelos/index.html',{
          "vuelos": Vuelo.objects.all()
     })

def vuelo(request, vuelo_id):
     unVuelo = Vuelo.objects.get(id=vuelo_id)
     return render(request, 'vuelos/vuelo.html',{
          "vuelo": unVuelo
     })